from django.shortcuts import render
from django.http import JsonResponse


def test(request):
    return JsonResponse({"success":True})


def home(request):
    if request.method == "GET":
        return render(request, "tracker.html", context={"form_id":"HI"})
    elif request.method == "POST":
        print("POST")
        #print(request.POST)
        courier = request.POST.get("courier")
        trackingno = request.POST.get("trackingno")
        print("courier:",courier)
        print("trackingno:",trackingno)

        response = find_courier(courier, trackingno, request)
        # response = find_courier("FedEx", "7723071445301", request)
        # return JsonResponse({"success":True, "data": response})
        return response
    else:
        return JsonResponse({"message":"Not allowed"})

from tracker.fedex_tracker import get_fedex

def find_courier(courier, trackingno, request):
    if courier == "FedEx":
        response = get_fedex(trackingno)
        # print("response:",response)
        # return response
        if response["isSuccessful"]:
            return render(request, "show_deatils.html", context=response)
        else:
            return render(request, "failed_tracking.html", context=response)
    else:
        return None