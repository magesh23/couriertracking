
from django.urls import path
from tracker import views
from tracker import errorhandler

urlpatterns = [
    path('test', views.test, name="Test"),
    path('', views.home, name="Home"),

    # error handling url
    path('404', errorhandler.handler404, name="404Page"),
    path('500', errorhandler.handler500, name="500Page"),
    path('403', errorhandler.handler403, name="403Page"),
    path('400', errorhandler.handler400, name="400Page"),
]

