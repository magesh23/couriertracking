import requests
import urllib

def fedex_tracker(tracking_number):
    headers = {
        'Accept': '/',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Host': 'www.fedex.com',
        'Origin': 'https://www.fedex.com',
        'Referer': 'https://www.fedex.com',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }
    def get_cookies():
        return dict(requests.get(url='https://www.fedex.com',headers=headers,verify=False).cookies)
    data = {"data": {"TrackPackagesRequest":{"appType":"WTRK","appDeviceType":"DESKTOP","supportHTML":"true","supportCurrentLocation":"true","uniqueKey":"","processingParameters":{},"trackingInfoList":[{"trackNumberInfo":{"trackingNumber":tracking_number,"trackingQualifier":"","trackingCarrier":""}}]}}}
    option = {"action": "trackpackages", "locale": "en_US", "version": "1", "format": "json"}
    form_data = urllib.parse.urlencode(data)+'&'+urllib.parse.urlencode(option)
    response = requests.post(url='https://www.fedex.com/trackingCal/track', headers=headers, data=form_data.replace('%27', '%22'), cookies=get_cookies(),verify=False)
    return response.json()


def get_fedex(tracking_number):
    response = fedex_tracker(tracking_number)
    # return response

    # isSuccessful
    isSuccessful = response["TrackPackagesResponse"]["packageList"][0]["isSuccessful"]

    if not isSuccessful:
        data = {
            "isSuccessful": isSuccessful,
            "courier": "FedEx",
            "trackingno": tracking_number,
            "status": "",
            "message": "Data requested for this tracking number is not found!"
        }
        return data

    # checkpoints
    response_checkpoints = response["TrackPackagesResponse"]["packageList"][0]["scanEventList"]
    checkpoints = []
    for i in response_checkpoints:
        checkpoints.append({
            "status" : i["status"],
            "deatils" : i["scanDetails"],
            "location" : i["scanLocation"],
            "date" : i["date"],
            "time" : i["time"],
        })

    # Origin
    originCity = response["TrackPackagesResponse"]["packageList"][0]["originCity"]
    originStateCD =  response["TrackPackagesResponse"]["packageList"][0]["originStateCD"]
    originZip = response["TrackPackagesResponse"]["packageList"][0]["originZip"]

    origin = "{} {} {}".format(originCity,originStateCD,originZip)

    # destination
    destCity = response["TrackPackagesResponse"]["packageList"][0]["destLocationCity"]
    destStateCD = response["TrackPackagesResponse"]["packageList"][0]["destLocationStateCD"]
    destZip = response["TrackPackagesResponse"]["packageList"][0]["destLocationZip"]

    destination = "{} {} {}".format(destCity,destStateCD,destZip)

    # main status
    status = response["TrackPackagesResponse"]["packageList"][0]["keyStatus"]
    
    # ["data"]["TrackPackagesResponse"]["packageList"]["isDelivered"]
    # ["data"]["TrackPackagesResponse"]["packageList"]["isInTransit"]

    # detail status
    detailed_status = response["TrackPackagesResponse"]["packageList"][0]["statusWithDetails"]

    data = {
        "isSuccessful": isSuccessful,
        "courier": "FedEx",
        "trackingno": tracking_number,
        "status": status,
        "detailed_status": detailed_status,
        "checkpoints": checkpoints,
        "originCity": originCity,
        "originStateCD": originStateCD,
        "originZip": originZip,
        "origin": origin,
        "destCity": destCity,
        "destStateCD": destStateCD,
        "destZip": destZip,
        "destination": destination,
    }

    return data

